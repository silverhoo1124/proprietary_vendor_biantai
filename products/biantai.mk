# Google's general properties
PRODUCT_PROPERTY_OVERRIDES += \
    keyguard.no_require_sim=true \
    ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
    ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
    ro.com.google.clientidbase=android-google \
    ro.com.android.wifi-watchlist=GoogleGuest \
    ro.setupwizard.enterprise_mode=1 \
    ro.com.android.dateformat=MM-dd-yyyy

# Bring in camera effects
PRODUCT_COPY_FILES += \
    vendor/biantai/prebuilt/common/media/LMprec_508.emd:system/vendor/media/LMprec_508.emd \
    vendor/biantai/prebuilt/common/media/PFFprec_600.emd:system/vendor/media/PFFprec_600.emd

# Bring in all video files
$(call inherit-product-if-exists, frameworks/base/data/videos/VideoPackage2.mk)

# Build PA Lightbulb(replaces Torch)
PRODUCT_PACKAGES += Lightbulb

# Include overlays
PRODUCT_PACKAGE_OVERLAYS += vendor/biantai/overlay/common

# Include Google prebuilt LatinIME lib
PRODUCT_COPY_FILES += \
    vendor/biantai/prebuilt/common/lib/libjni_latinime.so:system/lib/libjni_latinime.so

# Include multi-lingual LatinIME dictionaries
PRODUCT_PACKAGE_OVERLAYS += vendor/biantai/overlay/dictionaries

# Use Google stock sounds
PRODUCT_PROPERTY_OVERRIDES += \
    ro.config.alarm_alert=Osmium.ogg \
    ro.config.notification_sound=Tethys.ogg \
    ro.config.ringtone=Titania.ogg

# Enable SIP+VoIP on all targets
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml
